const path = require('path')


module.exports = {
    entry: './src/js/main.js',
    watch: true,
    output: {
        path: path.resolve('./dist/js'),
        filename: 'main.js'
    },
    module: {
        rules: [
            {
                test:/\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    }
}