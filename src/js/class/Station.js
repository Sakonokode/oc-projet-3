class Station {

    constructor(data) {

        // Velib station infos to display
        this.infosToDisplay = ['name', 'address', 'available_bike_stands', 'available_bikes', 'status'];

        // Use to translate string
        this.translation = {
            name: 'Nom',
            address: 'Adresse',
            available_bike_stands: 'Places disponibles',
            available_bikes: 'Vélos disponibles',
            status: 'Status',
            position: 'Coordonnées',
            id: 'id'
        };

        // Set the station infos
        this.data = data;
    }

    // METHODS

    get name() {
        return this.data.name;
    }

    get address() {
        return this.data.address;
    }

    get id() {
        return this.data.id;
    }

    getTranslation(text) {
        return this.translation[text];

    }

    // Set velib data in session storage
    static setStations(data) {
        sessionStorage.setItem('stations', data);
    }

    // Get the velib data stored in session storage
    static getStations() {
        return JSON.parse(sessionStorage.getItem('stations'));
    }

    // Remove the velib data from session storage
    static removeData() {
        sessionStorage.removeItem('stations');
    }

    // Check if a bike is available at the station
    isBikeAvailable() {
        console.log("dans isBikeavailable", this.data.available_bikes);
        return this.data.available_bikes > 0;
    }

    // Check if the station is open
    isStationOpen() {
        return this.data.status === 'OPEN';
    }

    // Check if the station passes the validation
    check() {
        return this.isBikeAvailable() && this.isStationOpen();
    }

}

export default Station;