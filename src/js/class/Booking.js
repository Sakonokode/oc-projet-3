import Station from './Station';
import {formatTimer} from '../helper';


class Booking {

    constructor(station) {
        this.keepForMinutes = 1;
        this.dateTime = null;
        this.expireOn = null;
        this.station = new Station(station);
        this.signature = null;
    }

    // Set the the time settings of the reservation
    setTime() {
        this.dateTime = Date.now();
        this.expireOn = (this.keepForMinutes * 60 * 1000) + this.dateTime; // in ms
    }

    // Make a new reservation
    make() {

        // Set the time settings of the reservation
        this.setTime();

        // Store the reservation in sessionStorage, stringify to be able to store an object
        sessionStorage.setItem('reservation', JSON.stringify(this));

        // Display the reservation
        Booking.setHtml();

        // Log the reservation
        Booking.log();

        // Start the expire count down
        Booking.expire()
    }

    // Get the time left before expiration
    static getTimeLeftBeforeExpiration() {

        // Get Current reservation
        let current = Booking.getCurrent();

        // Return a time in secondes
        return Math.floor((current.expireOn - Date.now()) / 1000); // in sec

    }

    // Set the expire process for a reservation
    static expire() {

        let myModal = $('#exampleModalCenter');

        // Set an interval for the count down
        let interval = window.setInterval(function () {

            // Do not execute count down if there is no current reservation
            if (!Booking.isCurrent()) return false;

            // Format the time to the desire format
            let timer = formatTimer(Booking.getTimeLeftBeforeExpiration());

            // If the reservation expire
            if (timer === false) {
                clearInterval(interval);
                Booking.removeCurrent();
                //Hide modal buttons and set necessary infos to display
                myModal.find('#order-modal-button').toggle();
                myModal.find('#cancel-modal-button').toggle();
                myModal.find('.modal-title').text('Réservation annulée');
                myModal.find('.modal-text').text('Votre réservation est arrivée à expiration et a été annulée.');
                myModal.modal('show');
                Booking.resetReservationDetails();
            }

            Booking.setReservationExpire(timer);

        }, 1000);

    }

    // Display the reservation details
    static setHtml() {

        const station = Booking.getCurrentStation();

        const timer = formatTimer(Booking.getTimeLeftBeforeExpiration());

        Booking.setReservationDetails(station.name);

        Booking.setReservationExpire(timer);

    }

    // Set the reservation expire time
    static setReservationExpire(html) {
        return $('#reservation-status').html('Expire dans <span class="tag is-danger">' + html + '</span>');
    }

    // Set the reservation details
    static setReservationDetails(html) {
        return $('#countdown-status').html('1 vélo réservé à la station &laquo; ' + html + ' &raquo;');
    }

    static resetReservationDetails() {
        return $('#countdown-status').html('Les 20 minutes sont écoulées, votre réservation à été annulée.');
    }

    static resetReservationExpire() {
        return $('#reservation-status').html('Aucune réservation en cours');
    }

    static hideOldReservationHtmlDisplay() {
        return $('#countdown-status').html('');
    }

    // Check if there is a current reservation
    static isCurrent() {
        return sessionStorage.getItem('reservation') !== null;
    }

    // Get the current reservation
    static getCurrent() {
        return JSON.parse(sessionStorage.getItem('reservation'));
    }

    // Get the current velib instance
    static getCurrentStation() {
        return new Station(Booking.getCurrent().station.data);
    }

    // Remove the current reservation
    static removeCurrent() {
        sessionStorage.removeItem('reservation');
    }

    // Log the reservation
    static log() {
        console.log(Booking.getCurrent());
    }

}

export default Booking;