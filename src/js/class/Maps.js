class Maps {

    constructor(mapContainer, mapCenter) {

        this.mapContainer = mapContainer;
        this.mapCenter = mapCenter;

        this.clusterOptions = {
            gridSize: 100,
            minimumClusterSize: 5,
            styles:[{
                height: 600,
                width: 600,
                textSize: 26
            }]
        };

        // Init a new map instance
        this.map = new google.maps.Map(this.mapContainer, {
            center: this.mapCenter,
            scrollwheel: false,
            zoom: 13
        });
    }

    // Create a marker
    createMarker(stationPosition, station) {

        let openStation = {
            url: '../../../images/m1.png'
        };

        let closedStation = {
            url: '../../../images/m3.png'
        };

        let openStationWithFewBikes = {
            url: '../../../images/m2.png'
        };

        if ((station.available_bike_stands > 5)) {
            return new google.maps.Marker({
                position : stationPosition,
                icon : openStation
            });
        }
        else if (station.available_bike_stands > 0) {
            return new google.maps.Marker({
                position : stationPosition,
                icon : openStationWithFewBikes
            });
        }
        else  {
            return new google.maps.Marker({
                position : stationPosition,
                icon : closedStation
            });
        }


    }



}

export default Maps