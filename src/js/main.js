import Carousel from "./class/Carousel";
import stationController from "./stationController";
import bookingController from "./bookingController";


let onReady = function() {
    let carousel = document.querySelector('#carousel1');
    new Carousel(carousel, {
        slidesVisible: 1,
        slidesToScroll: 1,
        loop: true
    })
};

if (document.readyState !== 'loading') {
    onReady()
}

document.addEventListener('DOMContentLoaded', onReady);


stationController();
