import Booking from './class/Booking';

export default function (station) {

    // Init variables
    let myModal = $('#exampleModalCenter');
    let orderButton = $('#order-station-button');
    let validateButton = $('#validate-order-button');
    let cancelButton = $('#cancel-order-button');
    let modalOrderButton = $('#order-modal-button');
    let modalCancelButton = $('#cancel-modal-button');
    let reservation = null;
    let canvas = document.querySelector("canvas");
    let signaturePad = new SignaturePad(canvas);
    let signature = $('.signature');
    let footerCancelButton = $('#footer-cancel-button');

    function toggleSignature() {
        // show signature container
        signature.toggle();

        // hide order button
        orderButton.toggle();
    }

    // Alert an error
    function displayError() {

        if (sessionStorage.currentStationStatus === 'CLOSE') {
            new Popup({
                title: 'La station est fermée',
                content: 'Vous ne pouvez pas réserver de vélo à cette station car celle-ci est fermée.'
            }).open();
            return false;
        }

        if (!(sessionStorage.currentAvailableBikes > 0)) {
            new Popup({
                title: 'Aucun vélo disponible',
                content: 'Vous ne pouvez pas réserver de vélo à cette station car aucun vélo n\'est disponible.'
            }).open();
            return false;
        }
    }

    function processEnd() {
        toggleSignature();
        signaturePad.clear();
    }


    // If there is a current reservation
    if (Booking.isCurrent()) {
        Booking.log();
        Booking.setHtml();
        Booking.expire();
    }

    // Use of unbind method to clear all precedents events in queue
    orderButton.unbind('click').click(function (e) {
        e.preventDefault();
        // If there are enough bikes and station is OPEN
        if ((sessionStorage.currentAvailableBikes > 0) && (sessionStorage.currentStationStatus === 'OPEN')) {
            toggleSignature();
        }
        else {
            displayError();
        }
    });

    // If we don’t use e.preventDefault(); to stop the default behavior, a # will be added to the URL in the address bar
    cancelButton.unbind('click').click(function (e) {
        e.preventDefault();
        signaturePad.clear();
        toggleSignature();
    });

    footerCancelButton.unbind('click').click(function (e) {
        e.preventDefault();
        Booking.removeCurrent();
        signaturePad.clear();
        Booking.resetReservationExpire();
        Booking.hideOldReservationHtmlDisplay();
    });

    validateButton.unbind('click').click(function (e) {
       e.preventDefault();

       if (signaturePad.isEmpty()) {
           // Edit modal's fields and show it
           myModal.find('.modal-title').text('Attention ?');
           myModal.find('.modal-text').text('Vous devez signer votre réservation.');
           myModal.modal('show');
       }

       // new instance of Booking
        reservation = new Booking(station);

        // Push the signature to the reservation
        reservation.signature = signaturePad.toDataURL();

        // If there is already an order in progress
        if (Booking.isCurrent()) {

            // Edit modal's fields and show it
            myModal.find('.modal-title').text('Que souhaitez vous faire ?');
            myModal.find('.modal-text').text('Vous ne pouvez avoir qu\'une seule réservation à la fois et une réservation est déjà en cours.');
            myModal.modal('show');

            // Add listeners on modal's buttons
            modalOrderButton.unbind('click').click(function (e) {
                e.preventDefault();
                reservation.make();
                processEnd();
                myModal.modal('hide');
            });

            modalCancelButton.unbind('click').click(function (e) {
                e.preventDefault();
                processEnd();
                myModal.modal('hide');
            });
        }

        else {
            reservation.make();
            processEnd();
        }

    });


}