// Exécute un appel AJAX GET
// Prend en paramètres l'URL cible et la method de requete HTTP

export function ajaxCall(url, method, callback) {

    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 400)) {
            callback(xhr.responseText);
        }
        else {
            console.error(xhr.status + " " + xhr.statusText + " " + url);
        }
    };

    xhr.open(method, url, true);
    xhr.send(null);
}