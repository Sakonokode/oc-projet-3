import Maps from './class/Maps';
import Station from './class/Station';
import {ajaxCall} from "./ajax";
import bookingController from "./bookingController";

function stationController() {

    // Init the variables
    const mapContainer = document.getElementById('google-map');
    const mapCenter = {lat: 45.764043, lng: 4.835658999999964};
    const maps = new Maps(mapContainer, mapCenter);
    const map = maps.map;
    const url = "https://api.jcdecaux.com/vls/v1/stations";
    const apiKey = "c09a80905cab53cf1d63b04b29ebb1c031d72b2f";
    const contractName = "?contract=" + "Lyon";
    const stationListApiUrl = url + "/" + contractName + "&apiKey=" + apiKey;
    const body = $('body');
    const signature = $('div.signature');
    const orderButton = $('body > div > div:nth-child(2) > div.col-md-4 > div.button-container > button');

    orderButton.toggle();
    signature.toggle();
    let stations = Station.getStations();

    if (stations === null) {

        // Ajax call to get the stations
        ajaxCall(stationListApiUrl, "GET", function (data) {

            // parse the stations data
            stations = JSON.parse(data);

            // Store the velib data in the session storage
            Station.setStations(data);

            // display markers on the map
            displayMarkers(stations);
        });
    }
    else {
        displayMarkers(stations);
    }

    function displayMarkers(stations) {

        let markers = [];
        let currentStation = {};

        stations.forEach(station => {


            currentStation.marker = maps.createMarker(station.position, station);
            google.maps.event.addListener(currentStation.marker, 'click', function (e) {


                if (orderButton.css('display') === 'none'){
                    orderButton.toggle();
                }

                //hide signature
                signature.hide();

                // Center the map on marker
                map.panTo(e.latLng);

                // API Request and Display
                getAndDisplayStation(station.number);

            });

            // push markers in the array
            markers.push(currentStation.marker);
        });
        new MarkerClusterer(map, markers, maps.clusterOptions);
    }

    function getAndDisplayStation(currentStationNbr) {

        let stationApiUrl = url + "/" + currentStationNbr + contractName + "&apiKey=" + apiKey;
        let stationInfos = null;

        ajaxCall(stationApiUrl, 'GET', function (response) {

            stationInfos = JSON.parse(response);
            displayStation(stationInfos);
            if (!(stationInfos.available_bikes > 0 && stationInfos.status === 'OPEN')) {
                orderButton.toggle();
            }
            bookingController(stationInfos);
        });
    }

    function displayStation(stationInfos) {

        let data = $("#info-station-status");
        $("#info-station-name").text(stationInfos.name);

        if (stationInfos.status === "OPEN") {
            data.text("Station Ouverte");
            data.removeClass('label-success label-danger');
            data.addClass('label label-success');
            sessionStorage.currentStationStatus = "OPEN";
        }

        else {
            data.text("Station Fermée");
            data.removeClass('label-success label-danger');
            data.addClass('label label-danger');
            sessionStorage.currentStationStatus = "CLOSE";
        }

        $("#info-station-address").text(stationInfos.address);
        $("#info-station-available-stands").text(stationInfos.available_bike_stands);
        $("#info-station-available-bikes").text(stationInfos.available_bikes);

        sessionStorage.currentStationName = stationInfos.name;
        sessionStorage.currentStationAddress = stationInfos.address;
        sessionStorage.currentAvailableBikeStands = stationInfos.available_bike_stands;
        sessionStorage.currentAvailableBikes = stationInfos.available_bikes;

    }
}

export default stationController;
